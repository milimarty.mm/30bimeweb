import React, {Component} from 'react';

import {BrowserRouter, Route} from "react-router-dom";
import style from './App.css';
import MainPage from "./Views/MainPageAria/MainPage/MainPage";
import CarPage from "./Views/ContentPageAria/CarPage/CarPage";
import ManagerPage from "./Views/ContentPageAria/ManagerPage/ManagerPage";
import OverviewPage from "./Views/ContentPageAria/OverviewPage/OverviewPage";
import FirePage from "./Views/ContentPageAria/FirePage/FirePage";
import EnergyPage from "./Views/ContentPageAria/EnergyPage/EnergyPage";
import AirPage from "./Views/ContentPageAria/AirPage/AirPage";
import ResponsibilityPage from "./Views/ContentPageAria/ResponsibilityPage/ResponsibilityPage";
import PersonPage from "./Views/ContentPageAria/PersonPage/PersonPage";
import EngineerPage from "./Views/ContentPageAria/EngineerPage/EngineerPage";
import LifeInsurancePage from "./Views/ContentPageAria/LifeInsurancePage/LifeInsurancePage";

class App extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    {/*<Route exact path="/panel" render={() => {*/}
                    {/*    return( <head>*/}
                    {/*        <meta http-equiv = "refresh" content = "2; url = https://p.30bime.ir/login/" />*/}
                    {/*    </head>)*/}
                    {/*}}/>*/}
                    <Route exact path="/" render={() => {
                        return(<MainPage/>)
                    }}/>

                    <Route exact path="/car-insurance" render={() => {
                        return(<CarPage/>)
                    }}/>
                    <Route exact path="/managers" render={() => {
                        return(<ManagerPage/>)
                    }}/>
                    <Route exact path="/overview" render={() => {
                        return(<OverviewPage/>)
                    }}/>
                    <Route exact path="/fire-insurance" render={() => {
                        return(<FirePage/>)
                    }}/>
                    <Route exact path="/energy-insurance" render={() => {
                        return(<EnergyPage/>)
                    }}/>
                    <Route exact path="/air-insurance" render={() => {
                        return(<AirPage/>)
                    }}/>
                    <Route exact path="/cargo-insurance" render={() => {
                        return(<ResponsibilityPage/>)
                    }}/>
                    <Route exact path="/person-insurance" render={() => {
                        return(<PersonPage/>)
                    }}/>
                    <Route exact path="/engineering-insurance" render={() => {
                        return(<EngineerPage/>)
                    }}/>
                    <Route exact path="/life-insurance" render={() => {
                        return(<LifeInsurancePage/>)
                    }}/>


                </BrowserRouter>

            </div>
        );
    }
}

export default App;
