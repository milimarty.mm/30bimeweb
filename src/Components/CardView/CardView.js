import React, {Component} from 'react';
import style from './CardView.css'
export default class CardView extends Component {



    render() {
        return (
            <div style={{
                width:this.props.width,
                height:this.props.height,
                padding:this.props.padding,
                display: "inline-block",
                verticalAlign: "middle",

            }}>
            <div className={style.cardViewContainer}>
                <div className={style.logoContainer}>
                    <img className={style.logo} src={this.props.logo}/>
                </div>
                <div className={style.title}>
                    {this.props.title}
                </div>
                <div >
                   <p className={style.description}> {this.props.description}</p>

                </div>
            </div>
            </div>
        );
    }


}

