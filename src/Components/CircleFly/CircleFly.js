import React, {Component} from 'react';
import style from "./CircleFly.css";



export default class CircleFly extends Component {

    state={
        isMoving:false,
    };

    timer= () =>{

        setTimeout(()=>{
            this.setState({isMoving:!this.state.isMoving});
            // this.timer()
        },this.props.time)

    };

    render() {
        this.timer();
        return (
            <div style={{
                width:this.props.width,
                height:this.props.height,

            }}  className={style.container}>
                <div className={this.state.isMoving? style.circle: style.circle2}  >
                    <div
                        style={{
                            height:"25%"
                        }}
                    >

                    </div>

                    <div
                        style={{
                            height:"60%"
                        }}
                    >
                        <img className={style.logo} src={this.props.logo}/>
                    </div>

                    <div
                        style={{
                            height:"25%"
                        }}
                    >

                    </div>






                </div>

            </div>
        );
    }
}

