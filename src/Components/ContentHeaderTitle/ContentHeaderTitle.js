import React, {Component} from "react";
import style from "./ContentHeaderTitle.css";

export default class ContentHeaderTitle extends Component {


    render() {

        return (
            <div className={style.contentHeader}>
                {this.props.children}
            </div>
        )
    }

}