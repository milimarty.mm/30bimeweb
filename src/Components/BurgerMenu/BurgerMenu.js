import React, {Component} from "react";
import style from './BurgerMenu.css'
import {Col, Row} from "reactstrap";
import close from "../../assets/img/close.svg"
import logo from '../../assets/img/logo.svg'
import BurgerMenuItem from "../BurgerMenuItem/BurgerMenuItem";
import lamplogo from "../../assets/img/lamp.svg";
import managersLogo from "../../assets/img/managers.svg";
import help from "../../assets/img/help.svg";
import bimeomer from "../../assets/img/life-insurance.svg";
import car from "../../assets/img/car-insurance.svg";
import fire from "../../assets/img/insurance.svg";
import plane from "../../assets/img/safe-flight.svg";
import human from "../../assets/img/insurance_human.svg";
import checklistLogo from "../../assets/img/checklist.svg";
import bimeEnginer from "../../assets/img/cogwheel.svg";
import energy from "../../assets/img/insurance_energy.svg";

export default class BurgerMenu extends Component {

    state = {
        menu :[
            {
                title:"cmr سامانه صدرو آنلاین",
                link:"/panel",
                isSubMenu:false,
                subMenu:[]


            },
            {
                title:"درباره ما",
                link:"#",
                isSubMenu:true,
                subMenu:[
                    {
                        title: "چشم انداز ,اهداف, ماموریت ها",
                        description: "",
                        logo: lamplogo,
                        link: "/overview",
                        onClick: () => {
                        }
                    },
                    {
                        title: "مدیران",
                        description: "",
                        logo: managersLogo,
                        link: "/managers",
                        onClick: () => {
                        }
                    },
                    {
                        title: "مشاوران",
                        description: "/1",
                        logo: help,
                        link: "",
                        onClick: () => {
                        }
                    },


                ],

            },
            {
                title:"رشته های بیمه ای",
                link:"/asd",
                isSubMenu:true,
                subMenu:[
                    {
                        logo: bimeomer,
                        title: "عمر و سرمایه گذاری",
                        description: "این نوع بیمه این امکان را برای شما فراهم می آورد که با پس‌انداز مبالغ اندک، ضمن استفاده از پوشش‌های بیمه‌ای و کاهش تبعات سنگین حوادث " +
                            "و اتفاقات ناگوار، امکان سرمایه‌گذاری حق‌بیمه‌های پرداختی فراهم گردد.",
                        link: "/life-insurance",
                        onClick: () => {
                        }
                    },
                    {
                        logo: car,
                        title: "اتومبیل",
                        description: "گروه بیمه اتومبیل جبران کننده خسارت‌های ناشی از حوادث رانندگی از جمله تصادف، آتش‌سوزی" +
                            " و سرقت به اتومبیل مورد بیمه، سرنشینان آن و اشخاص ثالث می‌باشد.",
                        link: "/car-insurance",
                        onClick: () => {
                        }
                    },
                    {
                        logo: fire,
                        title: "آتش سوزی",
                        description: "بیمه آتش سوزی پوششی‌ است‌ که‌ خسارات‌ و زیان های‌ ناشی‌ از " +
                            "وقوع‌ آتش‌ سوزی‌ که‌ به‌ اموال‌ مورد بیمه وارد می‌ شود را بر اساس‌ شرایط‌ مندرج‌ در بیمه نامه جبران‌ می‌کند.",
                        link: "/fire-insurance",
                        onClick: () => {
                        }
                    },
                    {
                        logo: plane,
                        title: "باربری",
                        description: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                            " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
                        link: "/air-insurance",
                        onClick: () => {
                        }
                    },
                    {
                        logo: human,
                        title: "اشخاص",
                        description: "بیمه اشخاص شامل بیمه های درمانی، عمر و حوادث می باشد. در بیمه درمانی، بیمه‌گر متعهد می‌شود که هزینه‌های‌ " +
                            "درمانی‌ و بیمارستانی‌ هر یک‌ از بیمه‌ شدگان‌ که ‌براساس‌ شرایط قرارداد و با رعایت‌ فرانشیز توافق‌ شده‌ قابل‌ پرداخت‌ می‌باشد را جبران نماید.",
                        link: "/person-insurance",
                        onClick: () => {
                        }
                    },
                    {
                        logo: checklistLogo,
                        title: "مسئولیت",
                        description: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                            " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
                        link: "/cargo-insurance",
                        onClick: () => {
                        }
                    },
                    {
                        logo: bimeEnginer,
                        title: "مهندسی",
                        description: "بیمه های مسئولیت به طور مستقیم در تنظیم روابط اجتماعی افراد جامعه، شناخت افراد از حقوق و مسئولیتهای یکدیگر و تامین امنیت حرفه" +
                            " ای مشاغل و فعالیتها تاثیر گذار می باشد. بیمه‌های مهندسی برای تحت پوشش بیمه قرار دادن فعالیت مهندسین در رشته‌های گوناگون می‌باشد.",
                        link: "/engineering-insurance",
                        onClick: () => {
                        }

                    },
                    {
                        logo: energy,
                        title: "انرژی",
                        description: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                            " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
                        link: "/energy-insurance",
                        onClick: () => {
                        }
                    },
                ]

            },

            {
                title:"تماس با ما",
                link:"#",
                isSubMenu:false,


            },


        ]

    };

    render() {
        return (
            <div style={{
                height: "600px",
                width: "100%",
                overflowY:"scroll",
                overflowX:"hidden"
            }}>
                <div className={style.logoSection}>
                    <Row className={style.logoSectionBackground}>
                        <Col onClick={this.props.onCloseClick} className={style.closeBurgerMenu} xl={12} md={12} xs={12} sm={12} >
                            <div style={{
                                display:"inline-block",

                            }}>
                                <img  src={close} className={style.closeLogo}/>

                            </div>

                            <div style={{
                                display:"inline-block",
                                width:"50px",
                                fontSize:"19px",
                                fontWeight:"bold"
                            }}> بستن</div>
                        </Col>
                        <Col xs={12} sm={12}>
                            <img  src={logo} className={style.logo}/>
                            <Row>
                                <Col>

                                    <div style={{
                                        display:"inline-block",
                                        width:"auto",
                                        fontSize:"15px",
                                        color:"white"
                                    }}>کارگزاری رسمی بیمه مرکزی</div>
                                </Col>
                            </Row>
                        </Col>

                    </Row>
                </div>
                <div className={style.menuSection}>
                    {this.state.menu.map((item , index) =>{
                        return(<BurgerMenuItem onClick={item.onClick} isSubMenu={item.isSubMenu} link ={item.link} title={item.title} subMenuItem = {item.subMenu} />)
                    })}

                </div>

            </div>
        )
    }

}