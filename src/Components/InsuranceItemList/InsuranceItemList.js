import React , {Component} from "react";
import {Col, Row} from "reactstrap";
import MenuItem from "../MenuItem/MenuItem";
import bimeomer from "../../assets/img/life-insurance.svg";
import car from "../../assets/img/car-insurance.svg";
import fire from "../../assets/img/insurance.svg";
import plane from "../../assets/img/safe-flight.svg";
import human from "../../assets/img/insurance_human.svg";
import checklistLogo from "../../assets/img/checklist.svg";
import bimeEnginer from "../../assets/img/cogwheel.svg";
import energy from "../../assets/img/insurance_energy.svg";
import {withRouter} from "react-router";

 class InsuranceItemList extends Component{
     goUp(){
         window.scrollTo({
             top: 0,
             left: 0,
             behavior: 'smooth'
         });
     }
    state = {
        insurance: [
            {
                logo: bimeomer,
                title: "عمر و سرمایه گذاری",
                description: "این نوع بیمه این امکان را برای شما فراهم می آورد که با پس‌انداز مبالغ اندک، ضمن استفاده از پوشش‌های بیمه‌ای و کاهش تبعات سنگین حوادث " +
                    "و اتفاقات ناگوار، امکان سرمایه‌گذاری حق‌بیمه‌های پرداختی فراهم گردد.",
                link: "/life-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/life-insurance")
                }
            },
            {
                logo: car,
                title: "اتومبیل",
                description: "گروه بیمه اتومبیل جبران کننده خسارت‌های ناشی از حوادث رانندگی از جمله تصادف، آتش‌سوزی" +
                    " و سرقت به اتومبیل مورد بیمه، سرنشینان آن و اشخاص ثالث می‌باشد.",
                link: "/car-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/car-insurance")

                }
            },
            {
                logo: fire,
                title: "آتش سوزی",
                description: "بیمه آتش سوزی پوششی‌ است‌ که‌ خسارات‌ و زیان های‌ ناشی‌ از " +
                    "وقوع‌ آتش‌ سوزی‌ که‌ به‌ اموال‌ مورد بیمه وارد می‌ شود را بر اساس‌ شرایط‌ مندرج‌ در بیمه نامه جبران‌ می‌کند.",
                link: "/fire-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/fire-insurance")
                }
            },
            {
                logo: plane,
                title: "باربری",
                description: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                    " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
                link: "/-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/air-insurance")

                }
            },
            {
                logo: human,
                title: "اشخاص",
                description: "بیمه اشخاص شامل بیمه های درمانی، عمر و حوادث می باشد. در بیمه درمانی، بیمه‌گر متعهد می‌شود که هزینه‌های‌ " +
                    "درمانی‌ و بیمارستانی‌ هر یک‌ از بیمه‌ شدگان‌ که ‌براساس‌ شرایط قرارداد و با رعایت‌ فرانشیز توافق‌ شده‌ قابل‌ پرداخت‌ می‌باشد را جبران نماید.",
                link: "/person-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/person-insurance")
                }
            },
            {
                logo: checklistLogo,
                title: "مسئولیت",
                description: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                    " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
                link: "/cargo-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/cargo-insurance")
                }
            },
            {
                logo: bimeEnginer,
                title: "مهندسی",
                description: "بیمه های مسئولیت به طور مستقیم در تنظیم روابط اجتماعی افراد جامعه، شناخت افراد از حقوق و مسئولیتهای یکدیگر و تامین امنیت حرفه" +
                    " ای مشاغل و فعالیتها تاثیر گذار می باشد. بیمه‌های مهندسی برای تحت پوشش بیمه قرار دادن فعالیت مهندسین در رشته‌های گوناگون می‌باشد.",
                link: "/engineering-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/engineering-insurance")
                }

            },
            {
                logo: energy,
                title: "انرژی",
                description: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                    " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
                link: "/energy-insurance",
                onClick: () => {
                    this.goUp();
                    this.props.history.push("/energy-insurance")

                }
            },
        ]

    };
     i= 0;
     second=0;
     first = 1;
    test =(<div>
            {this.state.insurance.map((item , index ,array)=>{
                if(index===1)
                {
                   this.second=1;
                    this. first =2;
                }
                if(index>=4)
                    return ('');

                return(
                    <div onClick={this.aboutUs}
                         style={{
                             marginTop: "5px"
                         }}
                    >
                        <Row>

                            <Col>
                                <MenuItem  logo={array[index + this.second ].logo} title={array[index + this.second ].title}/>


                            </Col>

                            <Col>



                                <MenuItem logo={array[index +this.first+ this.second ].logo} title={array[index +this.first+ this.second ].title}/>

                            </Col>
                        </Row>


                    </div>

                )

            })}

        </div>)
    render() {

        return(
            <div>
                <div onClick={this.aboutUs}
                     style={{
                         marginTop: "5px"
                     }}
                >
                <Row>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[0].onClick} logo={this.state.insurance[0].logo} title={this.state.insurance[0].title}/>
                    </Col>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[1].onClick} logo={this.state.insurance[1].logo} title={this.state.insurance[1].title}/>
                    </Col>

                </Row>
                </div>
                <div onClick={this.aboutUs}
                     style={{
                         marginTop: "5px"
                     }}
                >
                <Row>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[2].onClick}  logo={this.state.insurance[2].logo} title={this.state.insurance[2].title}/>
                    </Col>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[3].onClick}  logo={this.state.insurance[3].logo} title={this.state.insurance[3].title}/>
                    </Col>

                </Row>
                </div>
                <div onClick={this.aboutUs}
                     style={{
                         marginTop: "5px"
                     }}
                >
                <Row>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[4].onClick}  logo={this.state.insurance[4].logo} title={this.state.insurance[4].title}/>
                    </Col>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[5].onClick}  logo={this.state.insurance[5].logo} title={this.state.insurance[5].title}/>
                    </Col>

                </Row>
                </div>
                <div onClick={this.aboutUs}
                     style={{
                         marginTop: "5px"
                     }}
                >
                <Row>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[6].onClick}  logo={this.state.insurance[6].logo} title={this.state.insurance[6].title}/>
                    </Col>

                    <Col>
                        <MenuItem  onClick={this.state.insurance[7].onClick}  logo={this.state.insurance[7].logo} title={this.state.insurance[7].title}/>
                    </Col>

                </Row>
                </div>

            </div>
        )
    }

}
export default withRouter(InsuranceItemList)