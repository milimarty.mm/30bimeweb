import React, {Component} from "react"
import style from "./BurgerMenuItem.css"
import {Col, Row} from "reactstrap";
import MenuItem from "../MenuItem/MenuItem";
import index from "eslint-plugin-jsx-a11y/src/util/implicitRoles";
import {Link} from "react-router-dom";

export default class BurgerMenuItem extends Component {
    state = {
        isSubMenuOPen: false
    };

    render() {
        return (
            <div>
                <div className={style.menuTitleContainer}>

                    <Link
                        onClick={this.props.isSubMenu ?
                            () => {
                                this.setState({isSubMenuOPen: !this.state.isSubMenuOPen})
                            } : null}
                        to={this.props.isSubMenu ? "#" : this.props.link}>
                        <div onClick={this.props.onClick} className={style.itemTitle}>
                            {this.props.title}
                        </div>
                        {
                            this.props.isSubMenu ?
                                <div className={style.itemLogoContainer}>
                                    <img
                                        className={this.state.isSubMenuOPen ? style.itemLogoOpen : style.itemLogoClose}/>
                                </div> : ''
                        }


                        <div
                            className={style.lineDiv}>
                        </div>
                    </Link>

                </div>
                <Row
                    className={this.state.isSubMenuOPen ? style.subMenuItemSectionOpen : style.subMenuItemSectionClose}>

                    {
                        this.props.isSubMenu ? this.props.subMenuItem.map((item, index) => {
                            return (<Col xs={12} sm={12} lg={12} md={12}
                                    style={{
                                        width: '80%',
                                        overflow: "hidden",
                                    }}>
                                    <Link onClick={item.onClick} to={item.link}>
                                        <MenuItem logo={item.logo}
                                                  title={item.title}/>
                                    </Link>

                                </Col>
                            )
                        }) : ''

                    }
                    <div
                        className={style.lineDiv}>
                    </div>
                </Row>


            </div>
        )
    }
}