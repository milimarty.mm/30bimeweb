import React, {Component} from 'react';
import style from './NavBar.css';
import logo from '../../assets/img/logo.svg'
import {Link} from "react-router-dom";
import ReactDOM from 'react-dom';
import accountLogoWhite from '../../assets/img/profile_white.svg'
import accountLogoPink from '../../assets/img/profile_pink.svg'
import MenuItem from "../MenuItem/MenuItem";
import managersLogo from "../../assets/img/managers.svg"
import help from "../../assets/img/help.svg"
import lamplogo from "../../assets/img/lamp.svg"
import InsuranceItemList from "../InsuranceItemList/InsuranceItemList";
import burgerMenuOpen from "../../assets/img/burger_menu.svg"
import BurgerMenu from "../BurgerMenu/BurgerMenu";
import {Redirect, withRouter} from "react-router";

class NavBar extends Component {
    i = 0;
    state = {
        headerOffset: null,
        pageYOffset: window.pageYOffset,
        profileLogo: false,
        isBurgerMenuOpen: false,
        aboutUsItem: [
            {
                title: "چشم انداز ,اهداف, ماموریت ها",
                description: "",
                logo: lamplogo,
                link: "/overview",
                onClick: () => {
                }
            },
            {
                title: "مدیران",
                description: "",
                logo: managersLogo,
                link: "/managers",
                onClick: () => {
                }
            },
            {
                title: "مشاوران",
                description: "/consultants",
                logo: help,
                link: "",
                onClick: () => {
                }
            },


        ],

    };
    contactUs = () => {
        window.scrollTo({
            top: 1000000,
            left: 0,
            behavior: 'smooth'
        });
    };
    aboutUs = () => {
        window.scrollTo({
            top: 1480,
            left: 0,
            behavior: 'smooth'
        });
    };
    home = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
        this.props.history.push('/')
    };
    openBurgerMenu = () => {
        this.setState({isBurgerMenuOpen: !this.state.isBurgerMenuOpen})
    };

    render() {
        return (
            <header className={this.state.pageYOffset > this.state.headerOffset ? style.sticky : ''}>

                <div className={style.account}>
                    <nav>
                        <ul>

                            <li>
                                <img className={style.accountLogo}
                                     src={this.state.profileLogo ? accountLogoPink : accountLogoWhite}/>

                                <a target="_blank"
                                   onMouseEnter={() => {
                                       this.setState({profileLogo: true});
                                   }}
                                   onMouseLeave={() => this.setState({profileLogo: false})}
                                   href="http://p.30bime.ir/login/"> cmr سامانه صدرو آنلاین</a>
                            </li>

                        </ul>
                    </nav>
                </div>
                <div className={style.container}>
                    <img onClick={this.home} src={logo} alt={"30bime-logo"} className={style.logo}/>

                    <img onClick={this.openBurgerMenu} src={burgerMenuOpen} alt={"30bime-logo"}
                         className={style.humbuergerMenuLogo}/>


                    <nav>
                        <ul>
                            <li><Link onClick={this.contactUs} to={'#'}>تماس با ما</Link></li>
                            <li><Link to={'#'}>
                                رشته های بیمه ای
                                <div className={[style.insurance, style.navMenu].join(' ')}>
                                    <div className={[style.insurance, style.navMenuItemSection].join(' ')}>
                                        <InsuranceItemList/>

                                    </div>
                                </div>
                            </Link></li>
                            <li><Link to={'#'}>درباره ما
                                <div className={[style.aboutUs, style.navMenu].join(' ')}>
                                    <div className={[style.aboutUs, style.navMenuItemSection].join(' ')}>
                                        {this.state.aboutUsItem.map((item, index) => {
                                            return (<div
                                                onClick={item.onClick}
                                                style={{
                                                    marginTop: "5px"
                                                }}>
                                                <Link to={item.link}>
                                                    <MenuItem
                                                        logo={item.logo}
                                                        title={item.title}
                                                    /></Link>
                                            </div>)
                                        })}
                                    </div>
                                </div>
                            </Link>
                            </li>
                            <li style={{
                                color: "white",
                                cursor: "default",
                                margin: "auto !important",
                                width: "100px !important"


                            }}>
                                کارگزاری رسمی بیمه مرکزی
                            </li>

                        </ul>

                    </nav>

                    <div
                        className={this.state.isBurgerMenuOpen ? style.burgerMenu : style.overflowHidden}
                    >
                        <BurgerMenu
                            onCloseClick={() => {
                                this.setState({isBurgerMenuOpen: !this.state.isBurgerMenuOpen})

                            }}>


                        </BurgerMenu>
                    </div>

                    <div className={style.mainMobileTitle}>
                        کارگزاری رسمی بیمه مرکزی
                    </div>
                </div>
            </header>
        );
    }


    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    };

    handleScroll = (event) => {

        let offset = ReactDOM.findDOMNode(this)
            .getBoundingClientRect();
        if (offset.y !== 0) {
            this.setState({headerOffset: offset.y});
        } else if (window.pageYOffset === 0) {
            this.setState({headerOffset: 0});
        }

    };

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

}

export default withRouter(NavBar)