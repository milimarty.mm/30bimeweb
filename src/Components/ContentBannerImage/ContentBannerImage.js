import React , {Component} from "react";
import {Col, Row} from "reactstrap";

export default class ContentBannerImage extends Component {

    render() {

        return(
            <Row className="justify-content-md-center">
                <Col
                    md={6}
                    lg={6}
                    style={{
                        margin:"auto",
                        padding:0
                    }} >

                    <img style={{
                        width: "100%",
                        height: "100%",
                        zIndex: "0",
                        padding: "0",
                        borderRadius: "20px",
                        boxShadow: "6px 6px 6px 6px rgba(0, 0, 0, 0.20)"

                    }} src={this.props.imageSrc}/>




                </Col>

            </Row>

        )

    }
}