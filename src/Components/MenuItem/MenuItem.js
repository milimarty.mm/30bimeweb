import React, {Component} from 'react';
import style from "./MenuItem.css";
import {Col, Container, Row} from "reactstrap";


export default class MenuItem extends Component {






    render() {

        return (
                <div onClick={this.props.onClick} style={{
                    padding:"2px"
                }} >


                <Container  className={style.itemContainer} >
                <Row>

                    <Col
                         md={9} lg={9} xs={9} sm={9} xl={9} >
                        <Row>
                            <Col  style={{
                                margin:"auto",
                                padding:"0"

                            }}>

                                <div className={style.title }>
                                    {this.props.title}
                                </div>
                            </Col>

                        </Row>
                        {/*<Row>*/}
                        {/*    <Col style={{*/}
                        {/*        margin:"0",*/}
                        {/*        padding:"0"*/}
                        
                        {/*    }}>*/}
                        
                        {/*        <div className={style.description }>*/}
                        {/*            12345*/}
                        {/*        </div>*/}
                        {/*    </Col>*/}
                        
                        {/*</Row>*/}

                    </Col>
                    <Col style={{
                        margin:"0",
                        marginTop:"auto",
                        marginBottom:"auto",
                        marginRight:"0",
                        marginLeft:"0",
                        padding:"0"

                    }}
                         md={3} lg={3} xs={3} sm={3} xl={3} >
                        <img className={style.itemLogo} src={this.props.logo} alt={"logo"} />
                    </Col>
                </Row>
                </Container>
                </div>
        );
    }


}

