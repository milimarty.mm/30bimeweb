import React, {Component} from 'react';
import style from "./FirstSection.css";
import NavBar from "../../../Components/navbar/NavBar";
import ReactDOM from "react-dom";


export default class FirstSection extends Component {


    render() {
        return (

            <div className={style.firstSection}>
                <div style={{
                    width: '100%',
                    height: '10%'
                }}>
                    <NavBar/>
                </div>

                <div className={style.textContainer}>
                    <div className={style.firstSectionTitle}>
                       سامانه صدور آنلاین CMR چیست؟
                    </div>

                    <div className={style.firstSectionDescription}>

                        سامانه صدور آنلاین cmr جهت راحتی شرکت های حمل نقل طراحی که توسط این سامانه می توان به صورت
                        آنلاین cmr خود را صادر و رهگیری کنید
                    </div>
                </div>
            </div>
        );
    }


}

