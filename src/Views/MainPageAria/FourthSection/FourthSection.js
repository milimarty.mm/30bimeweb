import React, {Component} from 'react';
import style from "./FourthSection.css";
import {Col, Container, Row} from "reactstrap";

import email from '../../../assets/img/email.svg'
import phoneLogo from '../../../assets/img/phone.svg'
import instagramPink from '../../../assets/img/instagram_pink.svg'
import instagramWhite from '../../../assets/img/instagram.svg'
import whatsappWhite from '../../../assets/img/whatsapp.svg'
import whatsappPink from '../../../assets/img/whatsapp_pink.svg'


export default class ThirdSection extends Component {

    state = {

        connectionLinks: [
            {
                name: "بیمه مرکزی جمهوری اسلامی ایران",
                link: "https://www.centinsur.ir/",
            },
            {
                name: "سندیکای بیمه گران ایران",
                link: "http://sbi.ir/",
            },
            {
                name: "پژوهشکده بیمه مرکزی",
                link: "http://www.irc.ac.ir/",
            },
            {
                name: "انجمن حرفه ای صنعت بیمه",
                link: "http://pii.ir/",
            },
            {
                name: "میانگین موزون سنا",
                link: "https://www.sanarate.ir/",
            },
        ]
        ,
        onInstagram: true,
        onWhatsApp: true,
    };


    social = (<div><Row>
        <Col md={6} xl={6} lg={6} sm={6} xs={6}>
            <div
                className={style.logoSocialContainer}
            >
                <img
                    className={style.logoSocial}
                    src={instagramWhite}/>
            </div>

        </Col>
        <Col
            style={{
                marginTop: "20px"
            }}
            md={6} xl={6} lg={6} sm={6} xs={6}>
            <Row>

                <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                    <div
                        className={style.socialContainer}
                    >
                        <a href="https://www.instagram.com/30bime.ir/"> Instagram</a>


                    </div>
                </Col>
            </Row>
        </Col>
    </Row><Row>
        <Col
            md={6} xl={6} lg={6} sm={6} xs={6}>
            <div
                className={style.logoSocialContainer}
            >
                <img
                    className={style.logoSocial}
                    src={whatsappWhite}/>
            </div>
        </Col>
        <Col
            style={{
                marginTop: "20px"
            }}
            md={6} xl={6} lg={6} sm={6} xs={6}>
            <Row>
                <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                    <div
                        className={style.socialContainer}
                    >
                        <a href="whatsapp://send?abid=021885371302&text=سلام">WhatsApp</a>

                    </div>
                </Col>
            </Row>
        </Col>
    </Row></div>);


    render() {
        let social1 = (

            <div>
                <div
                    className={style.logoSocialContainer}
                >
                    <a href="whatsapp://send?abid=021885371302&text=سلام"
                       target={"_blank"}>
                        <img
                            onMouseEnter={() => {
                                this.setState({onWhatsApp: false});
                            }}
                            onMouseLeave={() => {
                                this.setState({onWhatsApp: true});

                            }}
                            className={style.logoSocial}
                            src={this.state.onWhatsApp ? whatsappWhite : whatsappPink}
                        />
                    </a>

                </div>
                <div
                    className={style.logoSocialContainer}
                >
                    <a href="https://www.instagram.com/30bime.ir/"
                       target={"_blank"}>
                        <img
                            onMouseEnter={() => {
                                this.setState({onInstagram: false});
                            }}
                            onMouseLeave={() => {
                                this.setState({onInstagram: true});

                            }}
                            className={style.logoSocial}
                            src={this.state.onInstagram ? instagramWhite : instagramPink}
                        /></a>

                </div>
                <div
                    className={style.logoSocialContainer}
                >
                    <a target="_blank" href="https://trustseal.enamad.ir/?id=132328&amp;Code=O04QS9cfFQ1yiY6cYBF1"><img src="https://Trustseal.eNamad.ir/logo.aspx?id=132328&amp;Code=O04QS9cfFQ1yiY6cYBF1" alt={""} style={{cursor:"pointer",
                        width:"50px" ,
                        height:"50px"}} id={"O04QS9cfFQ1yiY6cYBF1"}/></a>

                </div>

            </div>


        );
        return (
            <div className={style.fourthSectionContainer}>
                <Container>
                    <Row className="justify-content-md-center">

                        <Col className={
                            style.firstRectangleContainer
                        } xs={12} sm={12} md={6} lg={6} xl={6}>
                            <div className={style.firstRectangle}>

                                <Container>

                                    <Row   className="justify-content-md-center" style={{
                                        marginTop: "20px"
                                    }}>
                                        <Col md={4} xl={4} lg={4} sm={4} xs={4}>
                                            <div
                                                className={style.rectangleLogoContainer}
                                            >
                                                <img
                                                    className={style.rectangleLogos}
                                                    src={email}/>
                                            </div>

                                        </Col>
                                        <Col md={8} xl={8} lg={8} sm={8} xs={8}>
                                            <Row>
                                                <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                                                    <div className={style.titleLogo}
                                                    >
                                                        ایمیل
                                                    </div>
                                                </Col>
                                                <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                                                    <div
                                                        className={style.email}
                                                    >
                                                        <a href="mailto:info@30bime.ir"> info@30bime.ir</a>

                                                    </div>
                                                </Col>
                                            </Row>


                                        </Col>


                                    </Row>
                                </Container>
                            </div>
                        </Col>
                        <Col className={
                            style.firstRectangleContainer
                        }
                            xs={12} sm={12} md={6} lg={6} xl={6}>
                            <div className={style.secondRectangle}>
                                <Container>

                                    <Row

                                        style={{
                                        marginTop: "20px"
                                    }}>
                                        <Col md={4} xl={4} lg={4} sm={4} xs={4}>
                                            <div
                                                className={style.rectangleLogoContainer}
                                            >
                                                <img
                                                    className={style.rectangleLogos}
                                                    src={phoneLogo}/>
                                            </div>

                                        </Col>
                                        <Col md={8} xl={8} lg={8} sm={8} xs={8}>
                                            <Row>
                                                <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                                                    <div className={style.titleLogo}>
                                                        تلفن
                                                    </div>
                                                </Col>
                                                <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                                                    <div className={style.phn}>
                                                        <a href="tel:021885371302"> 021-8853 7130-2</a>

                                                    </div>
                                                </Col>
                                            </Row>


                                        </Col>


                                    </Row>
                                </Container>

                            </div>
                        </Col>


                    </Row>
                    <Row>
                        <Col>
                            <div
                                style={{
                                    height: "50px"
                                }}
                            >

                            </div>
                        </Col>

                    </Row>
                    <Container>
                        <Row className={"justify-content-md-center"}>
                            <Col
                                style={{
                                    float: "right"
                                }}
                                sm={12}
                                xs={12}
                                md={4}
                                lg={4}
                                xl={4}
                            >

                                {/*{this.social}*/}

                                <Row>
                                    {this.state.connectionLinks.map((item) => {


                                        return (<Col className={style.connectionLinksContainer} md={12} xl={12} sm={12} xs={12} lg={12}>
                                            <a
                                                target="_blank"
                                                className={style.connectionLinks}
                                                href={item.link}>{item.name}</a>
                                        </Col>)

                                    })}


                                </Row>
                            </Col>
                            <Col
                                sm={12}
                                xs={12}
                                md={1}
                                lg={1}
                                xl={1}
                            >
                                <div
                                    className={style.lineDiv}>
                                </div>

                            </Col>

                            <Col

                                sm={12}
                                xs={12}
                                md={4}
                                lg={4}
                                xl={4}
                            >
                                <Container>

                                    <Row style={{
                                        marginTop: "20px"
                                    }}>


                                        <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                                            <div

                                                className={style.addressTitle}

                                            >
                                                آدرس دفتر مرکزی
                                            </div>
                                        </Col>
                                        <Col xl={12} sm={12} lg={12} md={12} xs={12}>
                                            <div

                                                className={style.addressDescription}
                                            >

                                                تهران : میدان آرژانتین، خيابان احمد قصير (بخارست)، خيابان شانزدهم غربی،
                                                شماره 13 واحد 4
                                            </div>
                                        </Col>


                                    </Row>
                                </Container>
                            </Col>


                        </Row>
                        <Row className="justify-content-md-center">
                            <Col md={12} xl={12} sm={12} xs={12} lg={12}>
                                {social1}
                            </Col>

                            <Col>
                                <div
                                    className={style.willowSection}
                                >
                                    <a href="http://willow-team.com"> Created In 30Bime Team</a>

                                </div>
                            </Col>
                            <Col md={12} xl={12} sm={12} xs={12} lg={12}>
                                <div
                                    style={{
                                        paddingTop:"15px",
                                        direction:"rtl"
                                    }}
                                    className={style.willowSection}
                                >
                                     کليه حقوق مادي و معنوي سايت متعلق به کارگزاری رسمی
                                        بیمه مرکزی کد 1405 بوده و استفاده از مطالب با ذکر منبع بلامانع است.

                                </div>
                            </Col>
                        </Row>

                    </Container>
                </Container>
            </div>
        );
    }


}

