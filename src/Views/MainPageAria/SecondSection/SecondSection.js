import React, {Component} from 'react';
import style from "./SecondSection.css";
import {Col, Row} from "react-bootstrap";
import CardView from "../../../Components/CardView/CardView";
import bimeomer from '../../../assets/img/life-insurance.svg'
import bimeEnginer from '../../../assets/img/cogwheel.svg'
import car from '../../../assets/img/car-insurance.svg'
import plane from '../../../assets/img/safe-flight.svg'
import fire from '../../../assets/img/insurance.svg'
import human from '../../../assets/img/insurance_human.svg'
import index from "eslint-plugin-jsx-a11y/src/util/implicitRoles";


export default class SecondSection extends Component {


    state = {
        card: [
            {
                logo: bimeomer,
                title: "عمر و سرمایه گذاری",
                desc: "این نوع بیمه این امکان را برای شما فراهم می آورد که با پس‌انداز مبالغ اندک، ضمن استفاده از پوشش‌های بیمه‌ای و کاهش تبعات سنگین حوادث " +
                    "و اتفاقات ناگوار، امکان سرمایه‌گذاری حق‌بیمه‌های پرداختی فراهم گردد.",
            },
            {
                logo: car,
                title: "اتومبیل",
                desc: "گروه بیمه اتومبیل جبران کننده خسارت‌های ناشی از حوادث رانندگی از جمله تصادف، آتش‌سوزی" +
                    " و سرقت به اتومبیل مورد بیمه، سرنشینان آن و اشخاص ثالث می‌باشد.",
            }, {
                logo: bimeEnginer,
                title: "مسئولیت و مهندسی",
                desc: "بیمه های مسئولیت به طور مستقیم در تنظیم روابط اجتماعی افراد جامعه، شناخت افراد از حقوق و مسئولیتهای یکدیگر و تامین امنیت حرفه" +
                    " ای مشاغل و فعالیتها تاثیر گذار می باشد. بیمه‌های مهندسی برای تحت پوشش بیمه قرار دادن فعالیت مهندسین در رشته‌های گوناگون می‌باشد.",
            },
            {
                logo: human,
                title: "اشخاص",
                desc: "بیمه اشخاص شامل بیمه های درمانی، عمر و حوادث می باشد. در بیمه درمانی، بیمه‌گر متعهد می‌شود که هزینه‌های‌ " +
                    "درمانی‌ و بیمارستانی‌ هر یک‌ از بیمه‌ شدگان‌ که ‌براساس‌ شرایط قرارداد و با رعایت‌ فرانشیز توافق‌ شده‌ قابل‌ پرداخت‌ می‌باشد را جبران نماید.",
            }, {
                logo: fire,
                title: "آتش سوزی",
                desc: "بیمه آتش سوزی پوششی‌ است‌ که‌ خسارات‌ و زیان های‌ ناشی‌ از " +
                    "وقوع‌ آتش‌ سوزی‌ که‌ به‌ اموال‌ مورد بیمه وارد می‌ شود را بر اساس‌ شرایط‌ مندرج‌ در بیمه نامه جبران‌ می‌کند.",
            },
            {
                logo: plane,
                title: "باربری، هواپیما و کشتی",
                desc: "گسترش روز افزون تجارت، دخالت عوامل مختلف در یک فرآیند تجاری باعث آن گردیده که امروزه نظام تجاری ساز کار پیچیده و خاص خود را" +
                    " مطالبه نماید. ورود بیمه گران به این عرصه در جهت تسهیل و ایجاد اطمینان بیشتر بین طرفین تجاری است.",
            }
        ]

    };

    render() {
        return (

            <div className={style.secondSection}>
                <div className={style.secondSection}>
                    <Row className="justify-content-md-center">
                        {
                            this.state.card.map((item, index) => {
                                if (index > 2)
                                    return
                                else
                                    return (<Col   style={{
                                        paddingRight:"0",
                                        paddingLeft:"0"

                                    }}  xs={12} sm={12} lg={5} xl={3} md={8}>
                                        <div className={style.cardViewContainer}>
                                            <CardView logo={item.logo} width={"90%"} height={"300px"}
                                                      title={item.title}
                                                      description={item.desc}/>
                                        </div>

                                    </Col>)

                            })
                        }
                    </Row>
                    <br/>
                    <Row className="justify-content-md-center">
                        {
                            this.state.card.map((item, index) => {
                                if (index <= 2)
                                    return
                                else
                                    return (<Col   style={{
                                        paddingRight:"0",
                                        paddingLeft:"0"

                                    }} xs={12} sm={12} lg={5} xl={3} md={8}>
                                        <div className={style.cardViewContainer}>
                                            <CardView logo={item.logo} width={"90%"} height={"300px"}
                                                      title={item.title}
                                                      description={item.desc}/>
                                        </div>

                                    </Col>)

                            })

                        }
                    </Row>

                </div>
            </div>
        );
    }
}

