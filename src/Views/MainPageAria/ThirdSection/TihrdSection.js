import React, {Component} from 'react';
import style from "./ThirdSection.css";
import {Col, Container, Row} from "reactstrap";
import bimeomer from '../../../assets/img/life-insurance.svg'
import bimeEnginer from '../../../assets/img/cogwheel.svg'
import car from '../../../assets/img/car-insurance.svg'
import plane from '../../../assets/img/safe-flight.svg'
import fire from '../../../assets/img/insurance.svg'
import human from '../../../assets/img/insurance_human.svg'
import CircleFly from "../../../Components/CircleFly/CircleFly";


export default class ThirdSection extends Component {
    render() {
        return (
            <div className={style.thirdSectionContainer}>
                <Row className="justify-content-md-center">
                    <Col>

                        <div className={style.title}>
                            درباره ما
                        </div>
                        <div>
                            <p className={style.description}>
                                در شرایط اقتصادی، اجتماعی و فرهنگی کنونی کشور نقش بیمه و تاثیر آن بر حفظ سرمایه ها و
                                ایجاد امنیت و آرامش در فضای کسب و کار و اجتماع بر کسی پوشیده نیست. این کارگزاری به عنوان
                                یکی از کارگزاری های خلاق و پیشرو در صنعت بیمه کشور و وجود بازار رقابتی در نظر دارد تا
                                بحول و قوه الهی بتواند با استفاده از پیشرفته ترین فناوری اطلاعات و برخورداری از
                                کارشناسان خبره، متعهد و با تجربه خدمات شایان و در شان مشتری را ارائه و آرامش خاطر آنان
                                را فراهم نماید.
                                {<br/>}

                                {<div style={{
                                    marginTop:"10px",
                                    marginBottom:"10px",
                                }} className={style.title} > چشم انداز</div>}
                                ارائه خدمات الکترونیکی به ذینفعان با اختصاص سایت اختصاصی کارگزاری
                                {<br/>}

                                نوآوری و خلاقیت در جهت ارائه خدمات بیمه ای

                                {<br/>}
                                مشتری مداری

                            </p>
                        </div>


                        <Container>
                            <Row className="justify-content-md-center">

                                <Col  xs={0} sm={0} md={6} lg={6} xl={6}>
                                    <CircleFly time={500} width={"120px"} height={"120px"} logo={human}/>
                                </Col>
                                <Col  className={style.clear}
                                     xs={0} sm={0} md={6} lg={6} xl={6}>
                                    <div style={{
                                        float: "right"
                                    }}>

                                        <CircleFly time={1000} width={"150px" } height={"150px"} logo={plane}/>
                                    </div>
                                </Col>


                            </Row>
                        </Container>
                        <Row style={{
                            marginTop: "50px"
                        }} className= {[style.clear ,"justify-content-md-center"].join(" ")}>

                            <Col
                                xs={0} sm={0} md={6} lg={6} xl={6}>
                                <div style={{
                                    float: "left"
                                }}>
                                    <CircleFly time={450} width={"180px"} height={"180px"} logo={bimeomer}/>
                                </div>
                            </Col>
                            <Col
                                xs={0} sm={0} md={6} lg={6} xl={6}>

                                <CircleFly time={800} width={"120px"} height={"120px"} logo={car}/>
                            </Col>


                        </Row>
                    </Col>

                </Row>
            </div>
        );
    }
}

