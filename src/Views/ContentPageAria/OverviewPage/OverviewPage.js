import React, {Component} from "react";
import ContentPage from "../../Pages/ContentPage";
import wallpaper from "../../../assets/img/over_view.jpg"
import {Col, Row} from "reactstrap";
import style from './OverviewPage.css'


export default class OverviewPage extends Component {

    state = {
        content: [
            {
                title: "چشم انداز ، ماموریت و اهداف ",
                desc: () => {
                    let desc = "چشم انداز \n" +
                        "در شرایط اقتصادی ، اجتماعی و فرهنگی کنونی کشور نقش بیمه و تاثیر آن بر حفظ سرمایه ها و ایجاد امنیت و آرامش در فضای کسب و کار و اجتماع بر کسی پوشیده نیست. این کارگزاری به عنوان یکی از کارگزاریهای خلاق و پیشرو در بخش خصوصی صنعت و بیمه کشور رو فشردگی رقابتی موجود در بازار در نظر دارد تا بحول و قوت الهی بتواند با استفاده از پیشرفته ترین فناوری اطلاعات و برخورداری از کارشناسان خبره و متعهد و با تجربه خدمات شایان و در خور مشتری و آرامش خاطر آنان را فراهم و ارائه نماید و بخش قابل توجهی از سهم بازار را بخود اختصاص دهد .\n" +
                        "•\tآمادگی برای ارائه خدمات الکترونیکی به ذینفعان با اختصاص سایت اختصاصی کارگزاری\n" +
                        "•\tنوآوری و خلاقیت در جهت ارائه خدمات بیمه ایی\n" +
                        "•\tمشتری مداری\n" +
                        "•\tتعهد و و فاداری سازمانی";

                    return desc.split("\n");
                }

            },
            {
                title: "ماموریت و اهداف\n",
                desc: () => {
                    let desc ="•\tتمرکز در امور بیمه ایی بانکها ، مراکز صنعتی و اقتصادی ، بیمه های زندگی سرمایه گذاری و حمل و نقل\n" +
                        "•\tتامین آرامش خاطر و اطمینان برای مشتریان\n" +
                        "•\tانتخاب شرکتهای بیمه برتر کشور و رایزنی با آنان در جهت ارائه خدمات مطلوب و مورد نیاز مشتریان و جامعه\n" +
                        "•\tجذب و حفظ مشتریان\n" +
                        "•\tاستفاده از فناوری و اطلاعات\n" +
                        "•\tنوآوری و خلاقیت\n" +
                        "•\tمشاور مشتریان\n" +
                        "•\tارایه مشاوره امور حقوقی برای مشتریان\n" +
                        "•\tبرنامه ریزی و اقدام در جهت بدست آوردن رتبه کارگزاری برتر سطح کشور\n" +
                        "•\tمحیطی آرام و صمیمی و بدون تنش برای همکاری";
                    return desc.split("\n");
                }

            },

        ],


    };

    render() {
        return (

            <ContentPage imageCover={wallpaper} headerTitle={"چشم انداز "}>

                <Row className="justify-content-md-center">
                    <Col>

                        {this.state.content.map((item, index) => {

                            return <div>
                                <div className={style.lineDiv}></div>
                                <h4 className={style.title}>
                                    {item.title}
                                </h4>
                                {item.desc().map((item) => {

                                    return <p className={style.desc}>&nbsp;
                                        {item}
                                    </p>
                                })}
                            </div>

                        })
                        }
                    </Col>
                </Row>


            </ContentPage>

        )
    }
}