import React, {Component} from "react";
import {Container} from "reactstrap";
import NavBar from "../../Components/navbar/NavBar";
import FourthSection from "../MainPageAria/FourthSection/FourthSection";
import ContentBannerImage from "../../Components/ContentBannerImage/ContentBannerImage";
import ContentHeaderTitle from "../../Components/ContentHeaderTitle/ContentHeaderTitle";

export default class ContentPage extends Component {

    render() {
        return (
            <div style={{
                background: "white",
                width: "100%",
                height: "100%",
                overflowX:"hidden",
            }}>
                <div style={{
                    height:"120px",
                    width:"100%"
                }}>
                    <div style={{
                        width: "100%",
                        background:"rgba(38,38,38,0.91)",
                        height:"auto"

                    }}>
                        <NavBar/>
                    </div>
                </div>
                <div style={{

                    background: "white",
                    width: "100%",
                    height: "100%",
                    margin: "auto",
                    textAlign:"center",
                    alignItems:"center",
                    padding:"20px",
                    wordWrap:"break-word",



                }}>
                    <Container>
                        <ContentBannerImage imageSrc ={this.props.imageCover}/>
                        <ContentHeaderTitle>
                            {this.props.headerTitle}
                        </ContentHeaderTitle>

                        {this.props.children}
                    </Container>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <FourthSection/>

            </div>

        )
    }

}